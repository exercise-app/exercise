FROM openjdk:latest

WORKDIR /
COPY target/*.jar app.jar
CMD java -jar app.jar -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE
