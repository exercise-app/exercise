package com.applause.exercise.repositories.tester;

import com.applause.exercise.models.entities.Tester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TesterRepository extends JpaRepository<Tester, Long> {

    @Query(
            nativeQuery = true,
            value = "select distinct country from tester"
    )
    List<String> findAllCountries();


    @Query(
            nativeQuery = true,
            value = "select distinct t.* from tester t join bug b on t.tester_id = b.tester_id " +
                    "join tester_device_ref r on t.tester_id = r.tester_id where t.country in ?1 " +
                    "and r.device_id in ?2"
    )
    List<Tester> findTestersByCriteriaSortedByExperience(List<String> countries, List<Long> devices);
}
