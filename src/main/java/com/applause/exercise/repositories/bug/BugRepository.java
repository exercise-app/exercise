package com.applause.exercise.repositories.bug;

import com.applause.exercise.models.entities.Bug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BugRepository extends JpaRepository<Bug, Long> {

    @Query(
            nativeQuery = true,
            value = "select count(b.*) from bug b where b.tester_id = ?1 and b.device_id in ?2"
    )
    int countTesterExperience(Long testerId, List<Long> deviceIds);
}
