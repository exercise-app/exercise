package com.applause.exercise.services.device;

import com.applause.exercise.models.dtos.DeviceDto;
import com.applause.exercise.models.mappers.DeviceMapper;
import com.applause.exercise.repositories.device.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeviceService implements IDeviceService {

    private DeviceRepository deviceRepository;
    private DeviceMapper deviceMapper;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository, DeviceMapper deviceMapper) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
    }


    @Override
    public Optional<List<DeviceDto>> getAllDevices() {
        return Optional.of(this.deviceMapper.map(deviceRepository.findAll()));
    }
}
