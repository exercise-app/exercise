package com.applause.exercise.services.device;

import com.applause.exercise.models.dtos.DeviceDto;

import java.util.List;
import java.util.Optional;

public interface IDeviceService {

    Optional<List<DeviceDto>> getAllDevices();
}
