package com.applause.exercise.services.tester;

import com.applause.exercise.models.dtos.TesterDto;

import java.util.List;
import java.util.Optional;

public interface ITesterService {

    Optional<List<String>> getAllCountries();
    Optional<List<TesterDto>> getTestersByCriteriaSortedByExperience(List<String> countries, List<Long> devices);
}
