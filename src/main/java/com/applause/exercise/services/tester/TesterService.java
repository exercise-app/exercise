package com.applause.exercise.services.tester;

import com.applause.exercise.models.dtos.TesterDto;
import com.applause.exercise.models.entities.Tester;
import com.applause.exercise.models.mappers.TesterMapper;
import com.applause.exercise.repositories.bug.BugRepository;
import com.applause.exercise.repositories.tester.TesterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class TesterService implements ITesterService {

    private TesterRepository testerRepository;
    private BugRepository bugRepository;
    private TesterMapper testerMapper;

    @Autowired
    public TesterService(TesterRepository testerRepository, BugRepository bugRepository, TesterMapper testerMapper) {
        this.testerRepository = testerRepository;
        this.bugRepository = bugRepository;
        this.testerMapper = testerMapper;
    }

    public Optional<List<String>> getAllCountries() {
        return Optional.of(this.testerRepository.findAllCountries());
    }

    @Override
    public Optional<List<TesterDto>> getTestersByCriteriaSortedByExperience(List<String> countries, List<Long> devices) {
        List<Tester> testers = this.testerRepository.findTestersByCriteriaSortedByExperience(countries, devices);
        if (!testers.isEmpty()) {
            List<TesterDto> mappedTesters = this.testerMapper.map(testers);
            mappedTesters.forEach(tester -> tester.setExperience(this.bugRepository.countTesterExperience(tester.getTesterId(), tester.getDeviceIds())));
            mappedTesters.sort(Comparator.comparingInt(TesterDto::getExperience).reversed());
            return Optional.of(mappedTesters);
        }
        return Optional.empty();
    }
}
