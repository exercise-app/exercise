package com.applause.exercise.services.csv;

import com.applause.exercise.models.dtos.BugDto;
import com.applause.exercise.models.dtos.DeviceDto;
import com.applause.exercise.models.dtos.TesterDeviceRefDto;
import com.applause.exercise.models.dtos.TesterDto;
import com.applause.exercise.models.entities.Bug;
import com.applause.exercise.models.entities.Device;
import com.applause.exercise.models.entities.Tester;
import com.applause.exercise.models.mappers.BugMapper;
import com.applause.exercise.models.mappers.DeviceMapper;
import com.applause.exercise.models.mappers.TesterMapper;
import com.applause.exercise.repositories.bug.BugRepository;
import com.applause.exercise.repositories.device.DeviceRepository;
import com.applause.exercise.repositories.tester.TesterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
public class CsvImportService implements ICsvImportService {

    private final String BUGS_FILEPATH = "src/data/bugs.csv";
    private final String DEVICES_FILEPATH = "src/data/devices.csv";
    private final String TESTERS_FILEPATH = "src/data/testers.csv";
    private final String TESTER_DEVICE_REFS = "src/data/tester_device.csv";

    private BugRepository bugRepository;
    private DeviceRepository deviceRepository;
    private TesterRepository testerRepository;

    private BugMapper bugMapper;
    private DeviceMapper deviceMapper;
    private TesterMapper testerMapper;

    private CsvDataLoaderService csvDataLoader;

    @Autowired
    public CsvImportService(BugRepository bugRepository, DeviceRepository deviceRepository,
                            TesterRepository testerRepository, BugMapper bugMapper, DeviceMapper deviceMapper,
                            TesterMapper testerMapper, CsvDataLoaderService csvDataLoader) {
        this.bugRepository = bugRepository;
        this.deviceRepository = deviceRepository;
        this.testerRepository = testerRepository;
        this.bugMapper = bugMapper;
        this.deviceMapper = deviceMapper;
        this.testerMapper = testerMapper;
        this.csvDataLoader = csvDataLoader;
    }

    public boolean importDataFromCsvFiles() {
        if (!testerRepository.existsById(1L)) {
            importBugsFromCsvFile(BUGS_FILEPATH);
            importDevicesFromCsvFile(DEVICES_FILEPATH);
            importTestersFromCsvFile(TESTERS_FILEPATH, TESTER_DEVICE_REFS);
            return true;
        }
        return false;
    }

    public Optional<List<BugDto>> importBugsFromCsvFile(String filePath) {
        Path bugsPath = Paths.get(filePath);
        if (Files.exists(bugsPath)) {
            List<Bug> bugList = this.csvDataLoader.loadObjectList(Bug.class, filePath).stream()
                    .filter(bug -> bug.getBugId() != null).collect(Collectors.toList());
            return Optional.of(this.bugMapper.map(this.bugRepository.saveAll(bugList)));
        }
        return Optional.empty();
    }

    public Optional<List<DeviceDto>> importDevicesFromCsvFile(String filePath) {
        Path devicesPath = Paths.get(filePath);
        if (Files.exists(devicesPath)) {
            List<Device> deviceList = this.csvDataLoader.loadObjectList(Device.class, filePath).stream()
                    .filter(device -> device.getDeviceId() != null).collect(Collectors.toList());
            return Optional.of(this.deviceMapper.map(this.deviceRepository.saveAll(deviceList)));
        }
        return Optional.empty();
    }

    public Optional<List<TesterDto>> importTestersFromCsvFile(String filePathTesters, String filePathRefs) {
        Path testersPath = Paths.get(filePathTesters);
        if (Files.exists(testersPath)) {
            List<TesterDeviceRefDto> refs = this.csvDataLoader.loadObjectList(TesterDeviceRefDto.class, filePathRefs).stream()
                    .filter(ref -> ref.getTesterId() != null).collect(Collectors.toList());
            List<Tester> testerList = this.csvDataLoader.loadObjectList(Tester.class, filePathTesters).stream()
                    .filter(tester -> tester.getTesterId() != null).collect(Collectors.toList());
            testerList.forEach(tester -> {
                refs.stream().filter(ref -> ref.getTesterId().equals(tester.getTesterId())).forEach(ref -> tester.getDeviceIds().add(ref.getDeviceId()));
            });
            return Optional.of(this.testerMapper.map(this.testerRepository.saveAll(testerList)));
        }
        return Optional.empty();
    }

}
