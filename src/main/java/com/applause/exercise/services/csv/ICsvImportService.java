package com.applause.exercise.services.csv;

import com.applause.exercise.models.dtos.BugDto;
import com.applause.exercise.models.dtos.DeviceDto;
import com.applause.exercise.models.dtos.TesterDto;

import java.util.List;
import java.util.Optional;

public interface ICsvImportService {

    boolean importDataFromCsvFiles();
    Optional<List<BugDto>> importBugsFromCsvFile(String filePath);
    Optional<List<DeviceDto>> importDevicesFromCsvFile(String filePath);
    Optional<List<TesterDto>> importTestersFromCsvFile(String filePathTesters, String filePathRefs);
}
