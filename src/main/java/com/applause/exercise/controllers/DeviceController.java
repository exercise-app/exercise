package com.applause.exercise.controllers;

import com.applause.exercise.models.dtos.DeviceDto;
import com.applause.exercise.services.device.IDeviceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/device")
public class DeviceController {

    private IDeviceService deviceService;

    public DeviceController(IDeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping(value = "/all")
    public List<DeviceDto> getAllDevices() {
        return this.deviceService.getAllDevices()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
