package com.applause.exercise.controllers;

import com.applause.exercise.models.dtos.TesterDto;
import com.applause.exercise.services.tester.ITesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/tester")
public class TesterController {

    private ITesterService testerService;

    @Autowired
    public TesterController(ITesterService testerService) {
        this.testerService = testerService;
    }

    @GetMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAllCountries() {
        return this.testerService.getAllCountries()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TesterDto> getTestersByCriteriaSortedByExperience(@RequestParam("countries") List<String> countries, @RequestParam("devices") List<Long> devices) {
        return this.testerService.getTestersByCriteriaSortedByExperience(countries, devices)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
