package com.applause.exercise.controllers;

import com.applause.exercise.services.csv.ICsvImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/csv/import")
public class CsvImportController {

    private ICsvImportService csvImportService;

    @Autowired
    public CsvImportController(ICsvImportService csvImportService) {
        this.csvImportService = csvImportService;
    }

    @GetMapping()
    public ResponseEntity<Boolean> importDataFromCsvFiles() {
        return new ResponseEntity<>(this.csvImportService.importDataFromCsvFiles(), HttpStatus.OK);
    }

}
