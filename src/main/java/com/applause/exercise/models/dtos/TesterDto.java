package com.applause.exercise.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TesterDto {

    private Long testerId;
    private String firstName;
    private String lastName;
    private String country;
    private Date lastLogin;
    private int experience;
    private List<Long> deviceIds;
}
