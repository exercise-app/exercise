package com.applause.exercise.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BugDto {

    private Long bugId;
    private Long deviceId;
    private Long testerId;
}
