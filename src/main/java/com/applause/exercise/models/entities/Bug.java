package com.applause.exercise.models.entities;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Getter
@Setter
@Entity
@Table(name = "BUG")
public class Bug {

    @Id
    @NotNull
    @Column(name = "BUG_ID")
    private Long bugId;

    @NotNull
    @Column(name = "TESTER_ID")
    private Long testerId;

    @NotNull
    @Column(name = "DEVICE_ID")
    private Long deviceId;
}
