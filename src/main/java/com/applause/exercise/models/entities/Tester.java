package com.applause.exercise.models.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "TESTER")
public class Tester {

    @Id
    @NotNull
    @Column(name = "TESTER_ID")
    private Long testerId;

    @NotNull
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotNull
    @Column(name = "LAST_NAME")
    private String lastName;

    @NotNull
    @Column(name = "COUNTRY")
    private String country;

    @NotNull
    @Column(name = "LAST_LOGIN")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLogin;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "TESTER_DEVICE_REF", joinColumns = @JoinColumn(name = "TESTER_ID"))
    @Column(name = "DEVICE_ID")
    private Set<Long> deviceIds = new HashSet<>();
}
