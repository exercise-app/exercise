package com.applause.exercise.models.entities;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "DEVICE")
public class Device {

    @Id
    @NotNull
    @Column(name = "DEVICE_ID")
    private Long deviceId;

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description;
}
