package com.applause.exercise.models.mappers;

import com.applause.exercise.models.dtos.BugDto;
import com.applause.exercise.models.entities.Bug;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface BugMapper {

    Bug map(BugDto dto);
    BugDto map(Bug entity);
    List<BugDto> map(List<Bug> list);
}
