package com.applause.exercise.models.mappers;

import com.applause.exercise.models.dtos.TesterDto;
import com.applause.exercise.models.entities.Tester;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface TesterMapper {


    Tester map(TesterDto dto);

    @Mapping(target = "experience", ignore = true)
    TesterDto map(Tester entity);

    List<TesterDto> map(List<Tester> list);


}
