package com.applause.exercise.models.mappers;

import com.applause.exercise.models.dtos.DeviceDto;
import com.applause.exercise.models.entities.Device;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface DeviceMapper {

    Device map(DeviceDto dto);
    DeviceDto map(Device entity);
    List<DeviceDto> map(List<Device> list);
}
